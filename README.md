# esp32-vl53l0x
A VL53L0X driver for the ESP32

Based off of the [official VL53L0X API](http://www.st.com/content/st_com/en/products/embedded-software/proximity-sensors-software/stsw-img005.html)

Cloned from: [esp32-vl53l0x from Kyle Hendricks](https://github.com/kylehendricks/esp32-vl53l0x) and added CMakeLists.txt to be used with `idf.py` tool.
